require "uri"
require "net/http"
require "rexml/document"
require "openssl"

name = "<Firstname LastName>" #for XML file name
email = "<Firstname.Lastnane@Company.com>" #for Chrometa authentication
api_key = "<Chrometa api hash key>" #for Chrometa authentication
params = { "date" => "<MM>/<DD>/<YYYY>" } #what date to query

url = URI.parse("https://app.chrometa.com/api/report")
req = Net::HTTP::Post.new(url.path)
req.basic_auth email, api_key
req.set_form_data(params)

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

resp = http.start {|http| http.request(req) }

File.open(name + '.xml','w') do |s|
	s.puts resp.body 
end